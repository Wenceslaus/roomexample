package com.sourceit.roomexample.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Book(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val author: String,
    val name: String,
    val year: Int
)


