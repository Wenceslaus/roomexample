package com.sourceit.roomexample.db

import androidx.room.*
import io.reactivex.Flowable

@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(book: Book)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg books: Book)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(books: List<Book>)

    @Query("SELECT * FROM Book")
    fun selectAll(): List<Book>

    @Query("SELECT * FROM Book WHERE year >= 2000 AND year <= 2020 ORDER BY year DESC")
    fun selectModern(): Flowable<List<Book>>

    @Query("DELETE FROM Book")
    fun clearTable()
}