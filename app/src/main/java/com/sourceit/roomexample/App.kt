package com.sourceit.roomexample

import android.app.Application
import com.sourceit.roomexample.db.BookDataBase

class App : Application() {

    lateinit var db: BookDataBase

    override fun onCreate() {
        super.onCreate()
        db = BookDataBase.getInstance(this)
    }
}