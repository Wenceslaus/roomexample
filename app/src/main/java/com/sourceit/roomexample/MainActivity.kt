package com.sourceit.roomexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        main thread
//        val dao = (application as App).db.getBookDao()
//        dao.insert(Book(0, "author", "name", 2022))
//        dao.selectAll().forEach {
//            Log.i("book:", it.toString())
//        }


        //coroutines, different threads
//        GlobalScope.launch {
//            val dao = (application as App).db.getBookDao()
//            dao.insert(Book(0, "author", "name", 2020))
//            val list = dao.selectAll()
//            withContext(Dispatchers.Main) {
//                Log.i("book:", list.toString())
//            }
//        }

        val dao = (application as App).db.getBookDao()
        disposable = dao.selectModern()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Log.i("book:", it.toString())
                },{
                    it.printStackTrace()
                })

    }

    override fun onStop() {
        super.onStop()
        disposable?.dispose()
    }
}
